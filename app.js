var express = require('express');
var http = require('http');
var debug = require('debug')('app4');
var bodyParser = require('body-parser');
var app = express();
var path = require('path');
var cors = require('cors')

// use body parser
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

var routes = require('./routes/index');
app.use('/', routes);

var port = process.env.PORT || 3000;
var env = process.env.NODE_ENV || 'development';

var server = http.createServer(app);
server.listen(port, function () {
  console.log('RESTFull API Server Start On Port ' + server.address().port)
})
