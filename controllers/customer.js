var customers = require('../models/customers.js');

exports.create = async function(req, res, next) {
  var customersParams = {
    customerNumber: req.body.customerNumber,
    name: req.body.name,
    gender: req.body.gender,
    birthdate: req.body.birthdate,
    age: req.body.age,
    customerStatus: req.body.customerStatus,
    nationality: req.body.nationality,
    citizenship: req.body.citizenship,
    religion: req.body.religion,
    job: req.body.job,
    idCard: req.body.idCard,
    placeBirth: req.body.placeBirth,
    province: req.body.province,
    presentAddress: req.body.presentAddress,
    tel: req.body.tel,
    claim: req.body.claim
  }

  let addCustomer = await customers.add(customersParams);
  if (!addCustomer) {
      return res.status(500).json({ status:'error', message: 'Something gone wrong when creating customer' });
  }
  return res.status(200).json({ status:'success',data: addCustomer });

};

exports.getAll = async function(req, res, next) {
  var getAllCustomer = await customers.getAll();

  if (getAllCustomer == -1) {
      return res.status(500).json({ status:'error', message: 'Something gone wrong when get all customer' });
  } else if (getAllCustomer.length == 0) {
      return res.status(200).json({ status:'success', data: []});
  } else if (getAllCustomer.length > 0) {
      return res.status(200).json({ status:'success', data: getAllCustomer});
  }

};

exports.getById = async function(req, res, next) {
  var id = req.params.id;
  var customer = await customers.getById(id);

  if (customer.length <= 0) {
      return res.status(500).json({ status:'error', message: 'Customer not found' });
  }
  return res.status(200).json({ status:'success', data: customer[0] });
};

exports.update = async function(req, res, next) {
  var id = req.params.id; 
  
  var customersParams = {
      id: id,
      customerNumber: req.body.customerNumber,
      name: req.body.name,
      gender: req.body.gender,
      birthdate: req.body.birthdate,
      age: req.body.age,
      customerStatus: req.body.customerStatus,
      nationality: req.body.nationality,
      citizenship: req.body.citizenship,
      religion: req.body.religion,
      job: req.body.job,
      idCard: req.body.idCard,
      placeBirth: req.body.placeBirth,
      province: req.body.province,
      presentAddress: req.body.presentAddress,
      tel: req.body.tel,
      claim: req.body.claim  
  };

let checkCustomer = await customers.getById(id);
if (checkCustomer.length <= 0) {
    return res.status(500).json({ status:'error', message: 'Customer not found' });
}

let updateCustomer = await customers.update(customersParams);
//check user save error or not
if (!updateCustomer) {
    return res.status(500).json({ status:'error', message: 'Something gone wrong when updating customer' });
}
return res.status(200).json({ status:'success',data: updateCustomer });
};