var drugs = require('../models/drugs.js');

exports.create = async function (req, res, next) {
  var drugParams = {
    type: req.body.type,
    drugNumber: req.body.drugNumber,
    drugName: req.body.drugName,
    amount: req.body.amount,
    price: req.body.price,
    indicationsShort: req.body.indicationsShort,
    indicationsLong: req.body.indicationsLong,
    lot: req.body.lot,
    dateToBuy: req.body.dateToBuy,
    exp: req.body.exp
  }

  let addDrug = await drugs.add(drugParams);
  if (!addDrug) {
    return res.status(500).json({ status: 'error', message: 'Something gone wrong when creating drugs' });
  }
  return res.status(200).json({ status: 'success', data: addDrug });

};

exports.getAll = async function (req, res, next) {
  var getAllDrug = await drugs.getAll();
  if (getAllDrug == -1) {
    return res.status(500).json({ status: 'error', message: 'Something gone wrong when get All drug' });
  } else if (getAllDrug.length == 0) {
    return res.status(200).json({ status: 'success', data: [] });
  } else if (getAllDrug.length > 0) {
    return res.status(200).json({ status: 'success', data: getAllDrug });
  }
};

exports.getById = async function (req, res, next) {
  var id = req.params.id;
  var drug = await drugs.getById(id);

  if (drug.length <= 0) {
    return res.status(500).json({ status: 'error', message: 'drug not found' });
  }
  return res.status(200).json({ status: 'success', data: drug[0] });
};

// exports.getDrugByDrugNumber = async function (req, res, next) {
//   var drugNumber = req.body.drugNumber
//   var drug = await drugs.getDrugByDrugNumber(drugNumber);

//   if (drug.length <= 0) {
//     return res.status(500).json({ status: 'error', message: 'drug not found' });
//   }
//   return res.status(200).json({ status: 'success', data: drug[0] });
// };

exports.update = async function (req, res, next) {
  var id = req.params.id;

  var drugParams = {
    id: id,
    type: req.body.type,
    drugNumber: req.body.drugNumber,
    drugName: req.body.drugName,
    amount: req.body.amount,
    price: req.body.price,
    indicationsShort: req.body.indicationsShort,
    indicationsLong: req.body.indicationsLong,
    lot: req.body.lot,
    dateToBuy: req.body.dateToBuy,
    exp: req.body.exp
  };

  let checkDrug = await drugs.getById(id);
  if (checkDrug.length <= 0) {
    return res.status(500).json({ status: 'error', message: 'Drug not found' });
  }

  let updateDrug = await drugs.update(drugParams);
  //check user save error or not
  if (!updateDrug) {
    return res.status(500).json({ status: 'error', message: 'Something gone wrong when updating drug' });
  }
  return res.status(200).json({ status: 'success', data: updateDrug });
};

exports.delete = async function (req, res, next) {
  var id = req.params.id;
  let checkDrug = await drugs.getById(id);
  if (checkDrug.length <= 0) {
    return res.status(500).json({ status: 'error', message: 'drugs not found' });
  }
  let deleteDrug = await drugs.delete(id);
  //check user save error or not
  if (!deleteDrug) {
    return res.status(500).json({ status: 'error', message: 'Something gone wrong when deleting drug' });
  }
  return res.status(200).json({ status: 'success', data: deleteDrug });
};