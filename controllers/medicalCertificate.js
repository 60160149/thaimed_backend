var medicalCertificate = require('../models/medicalCertificate.js');

exports.createByCustomerId = async function(req, res, next) {
  var medicalCertificateParams = {
    customerId: req.params.id,
    userName: req.body.userName,
    certificateNumber: req.body.certificateNumber,
    presentAddress: req.body.presentAddress,
    dateToCome: req.body.dateToCome,
    onToDate: req.body.onToDate,
    customerName: req.body.customerName,
    symptoms: req.body.symptoms
  }

  let addMedicalCertificate = await medicalCertificate.add(medicalCertificateParams);
  if (!addMedicalCertificate) {
    return res.status(500).json({ status:'error', message: 'Something gone wrong when creating medical certificate' });
  }
  return res.status(200).json({ status:'success',data: addMedicalCertificate });
};