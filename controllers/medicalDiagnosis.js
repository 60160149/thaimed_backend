var medicalDiagnosis = require('../models/medicalDiagnosis.js');

exports.createByCustomerId = async function(req, res, next) {
  var medicalDiagnosisParams = {
    customerId: req.params.id,
    elementaryPrinciples: req.body.elementaryPrinciples,
    seasonalPrinciples: req.body.seasonalPrinciples,
    agePrinciples: req.body.agePrinciples,
    timePrinciples: req.body.timePrinciples,
    geographicalPlaceOfBirth: req.body.geographicalPlaceOfBirth,
    geographicalPresentAddress: req.body.geographicalPresentAddress,
    causeOfSymptoms: req.body.causeOfSymptoms,
    summaryOfSickness: req.body.summaryOfSickness,
    diagnosisBasedOfTheFourElements: req.body.diagnosisBasedOfTheFourElements,
    diseaseDiagnosis: req.body.diseaseDiagnosis,
    modernMedicalDiagnosis: req.body.modernMedicalDiagnosis,
    currentDate: req.body.currentDate
  }

  let addMedicalDiagnosis = await medicalDiagnosis.add(medicalDiagnosisParams);
  if (!addMedicalDiagnosis) {
    return res.status(500).json({ status:'error', message: 'Something gone wrong when creating medical diagnosis' });
  }
  return res.status(200).json({ status:'success',data: addMedicalDiagnosis });
};

exports.getAllByCustomerId = async function (req, res, next) {
  var customerId = req.params.id;
  var getAllMedicalDiagnosis = await medicalDiagnosis.getAllByCustomerId(customerId);

  if (getAllMedicalDiagnosis == -1) {
    return res.status(500).json({ status: 'error', message: 'Something gone wrong when get all medicalDiagnosis' });
  } else if (getAllMedicalDiagnosis.length == 0) {
    return res.status(200).json({ status: 'success', data: [] });
  } else if (getAllMedicalDiagnosis.length > 0) {
    return res.status(200).json({ status: 'success', data: getAllMedicalDiagnosis });
  }

};

exports.getById = async function (req, res, next) {
  var id = req.params.id;
  var getMedicalDiagnosis = await medicalDiagnosis.getById(id);

  if (getMedicalDiagnosis.length <= 0) {
    return res.status(500).json({ status: 'error', message: 'medicalDiagnosis not found' });
  }
  return res.status(200).json({ status: 'success', data: getMedicalDiagnosis[0] });
};

exports.update = async function (req, res, next) {
  var id = req.params.id;

  var medicalDiagnosisParams = {
    id: id,
    elementaryPrinciples: req.body.elementaryPrinciples,
    seasonalPrinciples: req.body.seasonalPrinciples,
    agePrinciples: req.body.agePrinciples,
    timePrinciples: req.body.timePrinciples,
    geographicalPlaceOfBirth: req.body.geographicalPlaceOfBirth,
    geographicalPresentAddress: req.body.geographicalPresentAddress,
    causeOfSymptoms: req.body.causeOfSymptoms,
    summaryOfSickness: req.body.summaryOfSickness,
    diagnosisBasedOfTheFourElements: req.body.diagnosisBasedOfTheFourElements,
    diseaseDiagnosis: req.body.diseaseDiagnosis,
    modernMedicalDiagnosis: req.body.modernMedicalDiagnosis
  }

  let checkMedicalDiagnosis = await medicalDiagnosis.getById(id);
  if (checkMedicalDiagnosis.length <= 0) {
    return res.status(500).json({ status: 'error', message: 'medicalDiagnosis not found' });
  }

  let updateMedicalDiagnosis = await medicalDiagnosis.update(medicalDiagnosisParams);
  //check user save error or not
  if (!updateMedicalDiagnosis) {
    return res.status(500).json({ status: 'error', message: 'Something gone wrong when updating medical diagnosis' });
  }
  return res.status(200).json({ status: 'success', data: updateMedicalDiagnosis });
};