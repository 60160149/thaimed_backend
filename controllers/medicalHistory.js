var medicalHistory = require('../models/medicalHistory.js');

exports.createByCustomerId = async function (req, res, next) {
  var medicalHistoryParams = {
    customerId: req.params.id,
    currentDate: req.body.currentDate,
    time: req.body.time,
    symptoms: req.body.symptoms,
    currentHistory: req.body.currentHistory,
    pastHistory: req.body.pastHistory,
    familyHistory: req.body.familyHistory,
    personalHistory: req.body.personalHistory
  }

  let addMedicalHistory = await medicalHistory.add(medicalHistoryParams);
  if (!addMedicalHistory) {
    return res.status(500).json({ status: 'error', message: 'Something gone wrong when creating medicalHistory' });
  }
  return res.status(200).json({ status: 'success', data: addMedicalHistory });
};

exports.getAllByCustomerId = async function (req, res, next) {
  var customerId = req.params.id;
  var getAllmedicalHistory = await medicalHistory.getAllByCustomerId(customerId);
  console.log("medicalHistory", getAllmedicalHistory);
  getAllmedicalHistory.map(it => {
    const date = new Date(it.currentDate)
    it.currentDate = date.toLocaleDateString('th-TH', {
      year: 'numeric',
      month: 'long',
      day: 'numeric',
    })
  })

  if (getAllmedicalHistory == -1) {
    return res.status(500).json({ status: 'error', message: 'Something gone wrong when get all customer' });
  } else if (getAllmedicalHistory.length == 0) {
    return res.status(200).json({ status: 'success', data: [] });
  } else if (getAllmedicalHistory.length > 0) {
    return res.status(200).json({ status: 'success', data: getAllmedicalHistory });
  }

};

exports.getById = async function (req, res, next) {
  var id = req.params.id;
  var getMedicalHistory = await medicalHistory.getById(id);
  console.log("getMedicalHistory", this.getMedicalHistory);
  if (getMedicalHistory.length <= 0) {
    return res.status(500).json({ status: 'error', message: 'MedicalHistory not found' });
  }
  return res.status(200).json({ status: 'success', data: getMedicalHistory[0] });
};



exports.update = async function (req, res, next) {
  var id = req.params.id;

  var medicalHistoryParams = {
    id: id,
    currentDate: req.body.currentDate,
    time: req.body.time,
    symptoms: req.body.symptoms,
    currentHistory: req.body.currentHistory,
    pastHistory: req.body.pastHistory,
    familyHistory: req.body.familyHistory,
    personalHistory: req.body.personalHistory
  }

  let checkMedicalHistory = await medicalHistory.getById(id);
  if (checkMedicalHistory.length <= 0) {
    return res.status(500).json({ status: 'error', message: 'MedicalHistory not found' });
  }

  let updateMedicalHistory = await medicalHistory.update(medicalHistoryParams);
  //check user save error or not
  if (!updateMedicalHistory) {
    return res.status(500).json({ status: 'error', message: 'Something gone wrong when updating medicine prescription' });
  }
  return res.status(200).json({ status: 'success', data: updateMedicalHistory });
};