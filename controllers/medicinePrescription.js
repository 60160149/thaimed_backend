const drugs = require('../models/drugs.js');
var medicinePrescription = require('../models/medicinePrescription.js');

exports.createByCustomerId = async function(req, res, next) {
  const data = await drugs.getById(req.params.drugId);
  const drug = data[0]

  drug.amount = drug.amount - req.body.amount

  try {
    await drugs.updateAmount(drug.amount, drug.id)
  } catch(e) {
    console.log('error', e);
  }
  

  var medicinePrescriptionParams = {
    customerId: req.params.id,
    type: req.body.type,
    drugNumber: req.body.drugNumber,
    drugName: drug.drugName,
    amount: req.body.amount,
    medicalHistoryId: req.body.medicalHistoryId
  }

  let addMedicinePrescription = await medicinePrescription.add(medicinePrescriptionParams);
  if (!addMedicinePrescription) {
    return res.status(500).json({ status:'error', message: 'Something gone wrong when creating medicine prescription' });
  }
  return res.status(200).json({ status:'success',data: addMedicinePrescription });
};

exports.getAllByCustomerId = async function(req, res, next) {
  var customerId = req.params.id; 
  var getAllMedicinePrescription = await medicinePrescription.getAll(customerId);

  if (getAllMedicinePrescription == -1) {
      return res.status(500).json({ status:'error', message: 'Something gone wrong when get all customer' });
  } else if (getAllMedicinePrescription.length == 0) {
      return res.status(200).json({ status:'success', data: []});
  } else if (getAllMedicinePrescription.length > 0) {
      return res.status(200).json({ status:'success', data: getAllMedicinePrescription});
  }

};

exports.getById = async function(req, res, next) {
  var id = req.params.id;
  var getMedicinePrescription = await medicinePrescription.getById(id);
  if (getMedicinePrescription.length <= 0) {
      return res.status(500).json({ status:'error', message: 'MedicalHistory not found' });
  }
  return res.status(200).json({ status:'success', data: getMedicinePrescription[0] });
};

exports.getMedicineList = async function(req, res, next) {
  var medicinePrescriptionParams = {
    customerId: req.params.customerId,
    medicalHistoryId: req.params.id
  }
  
  var getMedicineList = await medicinePrescription.getMedicineList(medicinePrescriptionParams);
  if (getMedicineList.length <= 0) {
      return res.status(500).json({ status:'error', message: 'MedicalHistory not found' });
  }
  return res.status(200).json({ status:'success', data: getMedicineList });
};


exports.update = async function (req, res, next) {
  var id = req.params.id;

  var medicinePrescriptionParams = {
    id: id,
    type: req.body.type,
    drugNumber: req.body.drugNumber,
    drugName: req.body.drugName,
    amount: req.body.amount,
    currentDate: req.body.currentDate
  };

  let checkMedicinePrescription = await medicinePrescription.getById(id);
  if (checkMedicinePrescription.length <= 0) {
    return res.status(500).json({ status: 'error', message: 'medicine prescription not found' });
  }

  let updateMedicinePrescription = await medicinePrescription.update(medicinePrescriptionParams);
  //check user save error or not
  if (!updateMedicinePrescription) {
    return res.status(500).json({ status: 'error', message: 'Something gone wrong when updating medicine prescription' });
  }
  return res.status(200).json({ status: 'success', data: updateMedicinePrescription });
};

exports.delete = async function (req, res, next) {
  var id = req.params.id;
  let checkMedicinePrescription = await medicinePrescription.getById(id);
  if (checkMedicinePrescription.length <= 0) {
    return res.status(500).json({ status: 'error', message: 'MedicinePrescription not found' });
  }
  let deleteMedicinePrescription = await medicinePrescription.delete(id);
  //check user save error or not
  if (!deleteMedicinePrescription) {
    return res.status(500).json({ status: 'error', message: 'Something gone wrong when deleting MedicinePrescription' });
  }
  return res.status(200).json({ status: 'success', data: deleteMedicinePrescription });
};