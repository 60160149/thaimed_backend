var physicalExamination = require('../models/physicalExamination.js');

exports.createByCustomerId = async function(req, res, next) {
  var physicalExaminationParams = {
    customerId: req.params.id,
    temp: req.body.temp,
    pulse: req.body.pulse,
    respirationRate: req.body.respirationRate,
    bp: req.body.bp,
    height: req.body.height,
    weight: req.body.weight,
    bmi: req.body.bmi,
    painScore: req.body.painScore,
    painfulArea: req.body.painfulArea,
    detailedPhysicalExamination: req.body.detailedPhysicalExamination,
    currentDate: req.body.currentDate
  }

  let addPhysicalExamination = await physicalExamination.add(physicalExaminationParams);
  if (!addPhysicalExamination) {
    return res.status(500).json({ status:'error', message: 'Something gone wrong when creating physical examination' });
  }
  return res.status(200).json({ status:'success',data: addPhysicalExamination });
};

exports.getAllByCustomerId = async function (req, res, next) {
  var customerId = req.params.id;
  var getAllPhysicalExamination = await physicalExamination.getAllByCustomerId(customerId);

  if (getAllPhysicalExamination == -1) {
    return res.status(500).json({ status: 'error', message: 'Something gone wrong when get all physicalExamination' });
  } else if (getAllPhysicalExamination.length == 0) {
    return res.status(200).json({ status: 'success', data: [] });
  } else if (getAllPhysicalExamination.length > 0) {
    return res.status(200).json({ status: 'success', data: getAllPhysicalExamination });
  }

};

exports.getById = async function (req, res, next) {
  var id = req.params.id;
  var getPhysicalExamination = await physicalExamination.getById(id);

  if (getPhysicalExamination.length <= 0) {
    return res.status(500).json({ status: 'error', message: 'physicalExamination not found' });
  }
  return res.status(200).json({ status: 'success', data: getPhysicalExamination[0] });
};

exports.update = async function (req, res, next) {
  var id = req.params.id;

  var physicalExaminationParams = {
    id: id,
    temp: req.body.temp,
    pulse: req.body.pulse,
    respirationRate: req.body.respirationRate,
    bp: req.body.bp,
    height: req.body.height,
    weight: req.body.weight,
    bmi: req.body.bmi,
    painScore: req.body.painScore,
    painfulArea: req.body.painfulArea,
    detailedPhysicalExamination: req.body.detailedPhysicalExamination
  }

  let checkPhysicalExamination = await physicalExamination.getById(id);
  if (checkPhysicalExamination.length <= 0) {
    return res.status(500).json({ status: 'error', message: 'physicalExamination not found' });
  }

  let updatePhysicalExamination = await physicalExamination.update(physicalExaminationParams);
  //check user save error or not
  if (!updatePhysicalExamination) {
    return res.status(500).json({ status: 'error', message: 'Something gone wrong when updating physical examination' });
  }
  return res.status(200).json({ status: 'success', data: updatePhysicalExamination });
};