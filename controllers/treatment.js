var treatments = require('../models/treatment.js');

exports.createByCustomerId = async function(req, res, next) {
  var treatmentsParams = {
    customerId: req.params.id,
    treatmentPlan: req.body.treatmentPlan,
    thaiHerbalCompressMassage: req.body.thaiHerbalCompressMassage,
    herbalSteam: req.body.herbalSteam,
    operative: req.body.operative,
    evaluationAfterTreatments: req.body.evaluationAfterTreatments,
    painScoreAf: req.body.painScoreAf,
    examinationDetails: req.body.examinationDetails,
    suggestions: req.body.suggestions,
    appointmentDate: req.body.appointmentDate,
    currentDate: req.body.currentDate
  }

  let addTreatments = await treatments.add(treatmentsParams);
  if (!addTreatments) {
      return res.status(500).json({ status:'error', message: 'Something gone wrong when creating treatment' });
  }
  return res.status(200).json({ status:'success',data: addTreatments });

};

exports.getAllByCustomerId = async function (req, res, next) {
  var customerId = req.params.id;
  var getAllTreatments = await treatments.getAllByCustomerId(customerId);

  if (getAllTreatments == -1) {
    return res.status(500).json({ status: 'error', message: 'Something gone wrong when get all treatments' });
  } else if (getAllTreatments.length == 0) {
    return res.status(200).json({ status: 'success', data: [] });
  } else if (getAllTreatments.length > 0) {
    return res.status(200).json({ status: 'success', data: getAllTreatments });
  }

};

exports.getById = async function (req, res, next) {
  var id = req.params.id;
  var getTreatments = await treatments.getById(id);

  if (getTreatments.length <= 0) {
    return res.status(500).json({ status: 'error', message: 'treatments not found' });
  }
  return res.status(200).json({ status: 'success', data: getTreatments[0] });
};

exports.update = async function (req, res, next) {
  var id = req.params.id;

  var treatmentsParams = {
    id: id,
    treatmentPlan: req.body.treatmentPlan,
    thaiHerbalCompressMassage: req.body.thaiHerbalCompressMassage,
    herbalSteam: req.body.herbalSteam,
    operative: req.body.operative,
    evaluationAfterTreatments: req.body.evaluationAfterTreatments,
    painScore: req.body.painScore,
    examinationDetails: req.body.examinationDetails,
    suggestions: req.body.suggestions,
    appointmentDate: req.body.appointmentDate,
  }

  let checkTreatments = await treatments.getById(id);
  if (checkTreatments.length <= 0) {
    return res.status(500).json({ status: 'error', message: 'treatments not found' });
  }

  let updateTreatments = await treatments.update(treatmentsParams);
  //check user save error or not
  if (!updateTreatments) {
    return res.status(500).json({ status: 'error', message: 'Something gone wrong when updating treatments' });
  }
  return res.status(200).json({ status: 'success', data: updateTreatments });
};