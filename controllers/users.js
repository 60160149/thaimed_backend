var users = require('../models/users.js');
var auth = require("../lib/auth");

exports.create = async function(req, res, next) {
    // var usr = req.body.username;
    // var passwd = req.body.password;
    // var name = req.body.name;

    // console.log(req.body);

    // if (typeof name == 'undefined') name = null;
    // if (typeof usr == 'undefined') usr = null;

    var usersParams = {
        name: req.body.name,
        username: req.body.username,
        password: req.body.password,
        email: req.body.email,
        status: req.body.status,
        certificateNumber: req.body.certificateNumber
    }


    //this validation better using library for more complex validation
    // if (!validateEmail(email)) {
    //     return res.status(400).json({ status:'fail', data: {email:'email invalid'}});
    // }

    // if (usr.length > 8 && usr.length < 4) {
    //     return res.status(400).json({ status:'fail', data: {username:'Username length minimum 4 max is 8 character!'}});
    // }

    // if (passwd.length < 6 && passwd.length > 8) {
    //     return res.status(400).json({ status:'fail', data: {password:'Password length minimum 6 max is 8 character!'}});
    // }

    var key = auth.setKey(req.body.username, req.body.password);
    usersParams.password = key;
    let getUser = await users.getUserByUsername(req.body.username);
    // check user exist
    if (getUser == -1) {
         return res.status(500).json({ status:'error', message: 'Something gone wrong when get user by username' });
    } else if (getUser.length > 0) {
         return res.status(400).json({ status:'fail',data: {username:'username is exist, please select unique username!'}});
    }
    let addUser = await users.add(usersParams);
    //check user save error or not
    if (!addUser) {
        return res.status(500).json({ status:'error', message: 'Something gone wrong when creating user' });
    }
    return res.status(200).json({ status:'success',data: addUser });
};

exports.update = async function(req, res, next) {
    var id = req.params.id;
    

    // if (typeof email == 'undefined') email = null;

    var usersParams = {
        id: id,
        name: req.body.name,
        username: req.body.username,
        password: req.body.password,
        email: req.body.email,
        status: req.body.status,
        certificateNumber: req.body.certificateNumber
    };

    //this validation better using library for more complex validation
    // if (!validateEmail(email)) {
    //     return res.status(400).json({ status:'fail', data: {email:'email invalid'}});
    // }
    let checkUser = await users.getById(id);
    if (checkUser.length <= 0) {
        return res.status(500).json({ status:'error', message: 'User not found' });
    }

    let updateUser = await users.update(usersParams);
    //check user save error or not
    if (!updateUser) {
        return res.status(500).json({ status:'error', message: 'Something gone wrong when updating user' });
    }
    return res.status(200).json({ status:'success',data: updateUser });
};

exports.delete = async function(req, res, next) {
    var id = req.params.id;
    let checkUser = await users.getById(id);
    if (checkUser.length <= 0) {
        return res.status(500).json({ status:'error', message: 'User not found' });
    }
    let deleteUser = await users.delete(id);
    //check user save error or not
    if (!deleteUser) {
        return res.status(500).json({ status:'error', message: 'Something gone wrong when deleting user' });
    }
    return res.status(200).json({ status:'success',data: deleteUser });
};

exports.getById = async function(req, res, next) {
    var id = req.params.id;
    var user = await users.getById(id);
    if (user.length <= 0) {
        return res.status(500).json({ status:'error', message: 'User not found' });
    }
    return res.status(200).json({ status:'success', data: user[0] });
};

exports.auth = async function(req, res, next) {
    var usr = req.body.username;
    var passwd = req.body.password;
    var key = auth.setKey(usr,passwd);
    var getUser = await users.getUserByKey(key);
    console.log("key", key);
    if (getUser == -1) {
        return res.status(500).json({ status:'error', message: 'Something gone wrong when get user by key' });
    } else if(getUser.length == 0) {
        return res.status(500).json({ status:'error', message: 'Not Authorized' });
    } else if(getUser.length > 0) {
        return res.status(200).json({ status:'success',data: {token:getUser[0].password, username: usr, name: getUser[0].name, status: getUser[0].status, certificateNumber: getUser[0].certificateNumber}});
    } 
    return res.status(200).json({ status:'success',data: 'Unhandled' });
};

exports.getAll = async function(req, res, next) {
    var getAllUser = await users.getAll();

    getAllUser.map(it => {
        if(it.status === 'ADMIN') {
            return it.status = 'ผู้ดูแลระบบ'
        }
        else if(it.status === 'DOCTOR') {
            return it.status = 'เจ้าหน้าที่แพทย์แผนไทย'
        }
        else {
            return it.status = 'อาจารย์แพทย์แผนไทย'
        }
    })
      
    if (getAllUser == -1) {
        return res.status(500).json({ status:'error', message: 'Something gone wrong when get All user' });
    } else if(getAllUser.length == 0) {
        return res.status(200).json({ status:'success', data: []});
    } else if(getAllUser.length > 0) {
        return res.status(200).json({ status:'success',data: getAllUser});
    }
};



// function validateEmail(email) {
//   const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
//   return re.test(email);
// }
