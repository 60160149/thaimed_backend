var db = require("../connect_my");
var customers = {};
const tableName = 'customers';

customers.add = function (params) {
  return new Promise(resolve => {
    var sql = `INSERT INTO ` + tableName + 
    ` VALUES (null,"` + 
    params.customerNumber + `", "` + params.name + `", "` + params.gender + `", "` + params.birthdate + `", "` + params.age + `", "` + params.customerStatus + `", "` + params.nationality + `", "` + params.citizenship + `", "` + params.religion + `", "` + params.job + `", "` + params.idCard + `", "` + params.placeBirth + `", "` + params.province + `", "` + params.presentAddress + `", "` + params.tel + `", "` + params.claim + `")`;
    db.query(sql, function (err, result) {
      if (err) {
          console.log(err);
          resolve(false);
      }
      resolve(result);
    });
  });
}

customers.getAll = function() {
  return new Promise(resolve => {
      var sql = `select * from ` + tableName ;
      db.query(sql, function (err, result) {
          if (err) {
              console.log(err);
              resolve(-1);
          }
          resolve(result);
      });
  });
}

customers.getById = function(id) {
  return new Promise(resolve => {
      var sql = `select * from ` + tableName + ` where id=` + id;
      db.query(sql, function (err, result) {
          if (err) {
              console.log(err);
              resolve(-1);
          }
          resolve(result);
      });
  });
}

customers.update = function(params) {
  return new Promise(resolve => {
      var sql = `UPDATE ${tableName} set 
      customerNumber="${params.customerNumber}", 
      name="${params.name}",
      gender="${params.gender}",
      birthdate="${params.birthdate}",
      age="${params.age}",
      customerStatus="${params.customerStatus}",
      nationality="${params.nationality}",
      citizenship="${params.citizenship}",
      religion="${params.religion}",
      job="${params.job}",
      idCard="${params.idCard}",
      placeBirth="${params.placeBirth}",
      province="${params.province}",
      presentAddress="${params.presentAddress}",
      tel="${params.tel}",
      claim="${params.claim}"
      where id=${params.id}`;
      db.query(sql, function (err, result) {
          if (err) {
              console.log(err);
              resolve(false);
          }
          resolve(result);
      });
  });

}

module.exports = customers;