var db = require("../connect_my");
var drugs = {};
const tableName = 'drug';

drugs.add = function (params) {
  return new Promise(resolve => {
    var sql = `INSERT INTO ` + tableName + 
    ` VALUES (null,"` + 
    params.type + `", "` +
    params.drugNumber + `", "` +
    params.drugName + `", "` + 
    params.amount + `", "` + 
    params.price + `", "` + 
    params.indicationsShort + `", "` + 
    params.indicationsLong	 + `", "` + 
    params.lot + `", "` + 
    params.dateToBuy + `", "` + 
    params.exp + `")`;
    db.query(sql, function (err, result) {
      if (err) {
        console.log(err);
        resolve(false);
    }
    resolve(result);
    });
  });
}

drugs.getAll = function() {
  return new Promise(resolve => {
      var sql = `select * from ` + tableName ;
      db.query(sql, function (err, result) {
          if (err) {
              console.log(err);
              resolve(-1);
          }
          resolve(result);
      });
  });
}

drugs.getById = function(id) {
  return new Promise(resolve => {
      var sql = `select * from ` + tableName + ` where id=` + id;
      db.query(sql, function (err, result) {
          if (err) {
              console.log(err);
              resolve(-1);
          }
          resolve(result);
      });
  });
}

drugs.updateAmount = function(amount, id) {
  return new Promise(resolve => {
    var sql = `UPDATE ${tableName} set amount="${amount}" where id=${id}`;
      db.query(sql, function (err, result) {
          if (err) {
              console.log(err);
              resolve(false);
          }
          resolve(result);
      });
    });
}


drugs.update = function(params) {
  return new Promise(resolve => {
      var sql = `UPDATE ${tableName} set 
      type="${params.type}", 
      drugNumber="${params.drugNumber}",
      drugName="${params.drugName}",
      amount="${params.amount}",
      price="${params.price}",
      indicationsShort="${params.indicationsShort}",
      indicationsLong="${params.indicationsLong}",
      lot="${params.lot}",
      dateToBuy="${params.dateToBuy}",
      exp="${params.exp}"
      where id=${params.id}`;
      db.query(sql, function (err, result) {
          if (err) {
              console.log(err);
              resolve(false);
          }
          resolve(result);
      });
  });

}

drugs.delete = function(id) {
  return new Promise(resolve => {
      var sql = `DELETE from  ` + tableName + ` where id=` + id;
      db.query(sql, function (err, result) {
          if (err) {
              console.log(err);
              resolve(-1);
          }
          resolve(result);
      });
  });
}

module.exports = drugs;