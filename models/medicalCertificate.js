var db = require("../connect_my");
var medicalCertificate = {};
const tableName = 'medicalCertificate';

medicalCertificate.add = function (params) {
  return new Promise(resolve => {
    var sql = `INSERT INTO ` + tableName + 
    ` VALUES (null,"` +
    params.customerId + `", "` + 
    params.userName + `", "` +
    params.certificateNumber + `", "` + 
    params.presentAddress + `", "` + 
    params.dateToCome + `", "` + 
    params.onToDate + `", "` + 
    params.customerName + `", "` +  
    params.symptoms + `")`;
    db.query(sql, function (err, result) {
      if (err) {
        console.log(err);
        resolve(false);
    }
    resolve(result);
    });
  });
}

module.exports = medicalCertificate;