var db = require("../connect_my");
var medicalDiagnosis = {};
const tableName = 'medicalDiagnosis';

medicalDiagnosis.add = function (params) {
  return new Promise(resolve => {
    var sql = `INSERT INTO ` + tableName + 
    ` VALUES (null,"` +
    params.customerId + `", "` + 
    params.elementaryPrinciples + `", "` +
    params.seasonalPrinciples + `", "` + 
    params.agePrinciples + `", "` + 
    params.timePrinciples + `", "` + 
    params.geographicalPlaceOfBirth + `", "` + 
    params.geographicalPresentAddress + `", "` + 
    params.causeOfSymptoms + `", "` +
    params.summaryOfSickness + `", "` + 
    params.diagnosisBasedOfTheFourElements + `", "` +
    params.diseaseDiagnosis + `", "` +
    params.modernMedicalDiagnosis + `", "` +   
    params.currentDate + `")`;
    db.query(sql, function (err, result) {
      if (err) {
        console.log(err);
        resolve(false);
    }
    resolve(result);
    });
  });
}

medicalDiagnosis.getAllByCustomerId = function(customerId) {
  return new Promise(resolve => {
      var sql = `select * from ` + tableName +` where customerId=` + customerId + ` ORDER BY id DESC`;
      db.query(sql, function (err, result) {
          if (err) {
              console.log(err);
              resolve(-1);
          }
          resolve(result);
      });
  });
}

medicalDiagnosis.getById = function(id) {
  return new Promise(resolve => {
      var sql = `select * from ` + tableName + ` where id=` + id;
      db.query(sql, function (err, result) {
          if (err) {
              console.log(err);
              resolve(-1);
          }
          resolve(result);
      });
  });
}

medicalDiagnosis.update = function(params) {
  return new Promise(resolve => {
      var sql = `UPDATE ${tableName} set 
      elementaryPrinciples="${params.elementaryPrinciples}", 
      seasonalPrinciples="${params.seasonalPrinciples}",
      agePrinciples="${params.agePrinciples}",
      timePrinciples="${params.timePrinciples}",
      geographicalPlaceOfBirth="${params.geographicalPlaceOfBirth}",
      geographicalPresentAddress="${params.geographicalPresentAddress}",
      causeOfSymptoms="${params.causeOfSymptoms}",
      summaryOfSickness="${params.summaryOfSickness}",
      diagnosisBasedOfTheFourElements="${params.diagnosisBasedOfTheFourElements}",
      diseaseDiagnosis="${params.diseaseDiagnosis}",
      modernMedicalDiagnosis="${params.modernMedicalDiagnosis}"
      where id=${params.id}`;
      db.query(sql, function (err, result) {
          if (err) {
              console.log(err);
              resolve(false);
          }
          resolve(result);
      });
  });
}

module.exports = medicalDiagnosis;