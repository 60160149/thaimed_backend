var db = require("../connect_my");
var medicalHistory = {};
const tableName = 'medicalHistory';

medicalHistory.add = function (params) {
  return new Promise(resolve => {
    var sql = `INSERT INTO ` + tableName + 
    ` VALUES (null,"` + 
    params.customerId + `", "` +
    params.currentDate + `", "` + 
    params.time + `", "` + 
    params.symptoms + `", "` + 
    params.currentHistory + `", "` + 
    params.pastHistory + `", "` + 
    params.familyHistory + `", "` + 
    params.personalHistory + `")`;
    db.query(sql, function (err, result) {
      if (err) {
        console.log(err);
        resolve(false);
    }
    resolve(result);
    });
  });
}

medicalHistory.getAllByCustomerId = function(customerId) {
  return new Promise(resolve => {
      var sql = `select * ,m.id as medicalHistoryId from ` + tableName + 
      ` as m LEFT JOIN physicalExamination as p on m.currentDate = p.currentDate 
      LEFT JOIN medicalDiagnosis as d on m.currentDate = d.currentDate
      LEFT JOIN treatment as t on m.currentDate = t.currentDate` +
      ` where m.customerId=` + customerId + ` group BY m.currentDate ORDER BY medicalHistoryId DESC`;
      db.query(sql, function (err, result) {
          if (err) {
              console.log(err);
              resolve(-1);
          }
          resolve(result);
      });
  });
}

medicalHistory.getById = function(id) {
  return new Promise(resolve => {
      var sql = `select * from ` + tableName + ` where id=` + id;
      db.query(sql, function (err, result) {
          if (err) {
              console.log(err);
              resolve(-1);
          }
          resolve(result);
      });
  });
}

medicalHistory.update = function(params) {
  return new Promise(resolve => {
      var sql = `UPDATE ${tableName} set 
      currentDate="${params.currentDate}", 
      time="${params.time}",
      symptoms="${params.symptoms}",
      currentHistory="${params.currentHistory}",
      pastHistory="${params.pastHistory}",
      familyHistory="${params.familyHistory}",
      personalHistory="${params.personalHistory}"
      where id=${params.id}`;
      db.query(sql, function (err, result) {
          if (err) {
              console.log(err);
              resolve(false);
          }
          resolve(result);
      });
  });
}



module.exports = medicalHistory;