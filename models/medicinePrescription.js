var db = require("../connect_my");
var medicinePrescription = [];
const tableName = 'medicinePrescription';

medicinePrescription.add = function (params) {
  return new Promise(resolve => {
    var sql = `INSERT INTO ` + tableName + 
    ` VALUES (null,"` +
    params.customerId + `", "` + 
    params.type + `", "` +
    params.drugNumber + `", "` + 
    params.drugName + `", "` +
    params.amount + `", "` +   
    params.medicalHistoryId + `")`;
    db.query(sql, function (err, result) {
      if (err) {
        console.log(err);
        resolve(false);
    }
    resolve(result);
    });
  });
}

medicinePrescription.getAll = function(customerId) {
  return new Promise(resolve => {
      var sql = `select * from ` + tableName +` where customerId=` + customerId;
      db.query(sql, function (err, result) {
          if (err) {
              console.log(err);
              resolve(-1);
          }
          resolve(result);
      });
  });
}

medicinePrescription.getMedicineList = function(medicinePrescriptionParams) {
  return new Promise(resolve => {
      var sql = `select * from ` + tableName +` where customerId=` + medicinePrescriptionParams.customerId + ` and medicalHistoryId=` + medicinePrescriptionParams.medicalHistoryId;
      db.query(sql, function (err, result) {
          if (err) {
              console.log(err);
              resolve(-1);
          }
          resolve(result);
      });
  });
}

medicinePrescription.getById = function(id) {
  return new Promise(resolve => {
      var sql = `select * from ` + tableName + ` where id=` + id;
      db.query(sql, function (err, result) {
          if (err) {
              console.log(err);
              resolve(-1);
          }
          resolve(result);
      });
  });
}


medicinePrescription.update = function(params) {
  return new Promise(resolve => {
      var sql = `UPDATE ${tableName} set 
      type="${params.type}", 
      drugNumber="${params.drugNumber}",
      drugName="${params.drugName}",
      amount="${params.amount}"
      where id=${params.id}`;
      db.query(sql, function (err, result) {
          if (err) {
              console.log(err);
              resolve(false);
          }
          resolve(result);
      });
  });
}

medicinePrescription.delete = function(id) {
  return new Promise(resolve => {
      var sql = `DELETE from  ` + tableName + ` where id=` + id;
      db.query(sql, function (err, result) {
          if (err) {
              console.log(err);
              resolve(-1);
          }
          resolve(result);
      });
  });
}

module.exports = medicinePrescription;