var db = require("../connect_my");
var physicalExamination = {};
const tableName = 'physicalExamination';

physicalExamination.add = function (params) {
  return new Promise(resolve => {
    var sql = `INSERT INTO ` + tableName + 
    ` VALUES (null,"` +
    params.customerId + `", "` + 
    params.temp + `", "` +
    params.pulse + `", "` + 
    params.respirationRate + `", "` + 
    params.bp + `", "` + 
    params.height + `", "` + 
    params.weight + `", "` + 
    params.bmi + `", "` +
    params.painScore + `", "` + 
    params.painfulArea + `", "` +
    params.detailedPhysicalExamination + `", "` +   
    params.currentDate + `")`;
    db.query(sql, function (err, result) {
      if (err) {
        console.log(err);
        resolve(false);
    }
    resolve(result);
    });
  });
}

physicalExamination.getAllByCustomerId = function(customerId) {
  return new Promise(resolve => {
      var sql = `select * from ` + tableName +` where customerId=` + customerId + ` ORDER BY id DESC`;
      db.query(sql, function (err, result) {
          if (err) {
              console.log(err);
              resolve(-1);
          }
          resolve(result);
      });
  });
}


physicalExamination.getById = function(id) {
  return new Promise(resolve => {
      var sql = `select * from ` + tableName + ` where id=` + id;
      db.query(sql, function (err, result) {
          if (err) {
              console.log(err);
              resolve(-1);
          }
          resolve(result);
      });
  });
}

physicalExamination.update = function(params) {
  return new Promise(resolve => {
      var sql = `UPDATE ${tableName} set 
      temp="${params.temp}", 
      pulse="${params.pulse}",
      respirationRate="${params.respirationRate}",
      bp="${params.bp}",
      height="${params.height}",
      weight="${params.weight}",
      bmi="${params.bmi}",
      painScore="${params.painScore}",
      painfulArea="${params.painfulArea}",
      detailedPhysicalExamination="${params.detailedPhysicalExamination}"
      where id=${params.id}`;
      db.query(sql, function (err, result) {
          if (err) {
              console.log(err);
              resolve(false);
          }
          resolve(result);
      });
  });
}

module.exports = physicalExamination;
