var db = require("../connect_my");
var treatments = {};
const tableName = 'treatment';

treatments.add = function (params) {
  return new Promise(resolve => {
    var sql = `INSERT INTO ` + tableName + 
    ` VALUES (null,"` + 
    params.customerId + `", "` + 
    params.treatmentPlan + `", "` +
    params.thaiHerbalCompressMassage + `", "` + 
    params.herbalSteam + `", "` + 
    params.operative + `", "` + 
    params.evaluationAfterTreatments + `", "` + 
    params.painScoreAf + `", "` + 
    params.examinationDetails + `", "` +
    params.suggestions + `", "` +  
    params.appointmentDate + `", "` +
    params.currentDate + `")`;
    db.query(sql, function (err, result) {
      if (err) {
          console.log(err);
          resolve(false);
      }
      resolve(result);
    });
  });
}

treatments.getAllByCustomerId = function(customerId) {
  return new Promise(resolve => {
      var sql = `select * from ` + tableName +` where customerId=` + customerId + ` ORDER BY id DESC`;
      db.query(sql, function (err, result) {
          if (err) {
              console.log(err);
              resolve(-1);
          }
          resolve(result);
      });
  });
}

treatments.getById = function(id) {
  return new Promise(resolve => {
      var sql = `select * from ` + tableName + ` where id=` + id;
      db.query(sql, function (err, result) {
          if (err) {
              console.log(err);
              resolve(-1);
          }
          resolve(result);
      });
  });
}

treatments.update = function(params) {
  return new Promise(resolve => {
      var sql = `UPDATE ${tableName} set 
      treatmentPlan="${params.treatmentPlan}", 
      thaiHerbalCompressMassage="${params.thaiHerbalCompressMassage}",
      herbalSteam="${params.herbalSteam}",
      operative="${params.operative}",
      evaluationAfterTreatments="${params.evaluationAfterTreatments}",
      painScore="${params.painScore}",
      examinationDetails="${params.examinationDetails}",
      suggestions="${params.suggestions}",
      appointmentDate="${params.appointmentDate}"
      where id=${params.id}`;
      db.query(sql, function (err, result) {
          if (err) {
              console.log(err);
              resolve(false);
          }
          resolve(result);
      });
  });
}

module.exports = treatments;