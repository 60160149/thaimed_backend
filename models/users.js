var db = require("../connect_my");
var users = {};
const tableName = 'users';

users.migration = function() {

    var sql = `CREATE TABLE IF NOT EXISTS `+ tableName +`
    (
        id int NOT NULL AUTO_INCREMENT,
        name VARCHAR(100),
        username VARCHAR(8),
        password VARCHAR(255),
        email VARCHAR(100),
        status VARCHAR(20),
        certificateNumber VARCHAR(20),
        PRIMARY KEY (id)
    )`;

    db.query(sql, function (err, result) {
        if (err) {
            console.log(err);
        }
        console.log("users Table executed");
    });

    return true;
}

users.add = function(params) {
    return new Promise(resolve => {
        var sql = `INSERT INTO `+ tableName +` VALUES (null,"` + params.name + `", "`+ params.username +`", "`+ params.password +`", "`+ params.email +`", "`+ params.status +`", "`+ params.certificateNumber +`")`;
        db.query(sql, function (err, result) {
            if (err) {
                console.log(err);
                resolve(false);
            }
            resolve(result);
        });
    });

}

users.update = function(params) {
    return new Promise(resolve => {
        var sql = `UPDATE `+ tableName +` set name="` + params.name + `",username="`+ params.username + `",password="`+ params.password + `",email="`+ params.email + `",status="`+ params.status + `",certificateNumber="`+ params.certificateNumber + `" where id=` + params.id;
        db.query(sql, function (err, result) {
            if (err) {
                console.log(err);
                resolve(false);
            }
            resolve(result);
        });
    });

}

users.getUserByUsername = function(username) {
    return new Promise(resolve => {
        var sql = `select id from `+ tableName +` where username = "`+ username +`"`;
        db.query(sql, function (err, result) {
            if (err) {
                console.log(err);
                resolve(-1);
            }
            resolve(result);
        });
    });
}

users.getUserByKey = function(authKey) {
    return new Promise(resolve => {
        var sql = `select password, name, status, certificateNumber  from `+ tableName +` where password = "`+ authKey +`"`;
        db.query(sql, function (err, result) {
            if (err) {
                console.log(err);
                resolve(-1);
            }
            resolve(result);
        });
    });
}


users.getAll = function() {
    return new Promise(resolve => {
        var sql = `select * from ` + tableName ;
        db.query(sql, function (err, result) {
            if (err) {
                console.log(err);
                resolve(-1);
            }
            resolve(result);
        });
    });
}

users.getById = function(id) {
    return new Promise(resolve => {
        var sql = `select * from ` + tableName + ` where id=` + id;
        db.query(sql, function (err, result) {
            if (err) {
                console.log(err);
                resolve(-1);
            }
            resolve(result);
        });
    });
}

users.delete = function(id) {
    return new Promise(resolve => {
        var sql = `DELETE from  ` + tableName + ` where id=` + id;
        db.query(sql, function (err, result) {
            if (err) {
                console.log(err);
                resolve(-1);
            }
            resolve(result);
        });
    });
}

module.exports = users;
