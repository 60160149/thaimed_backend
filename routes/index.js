var express = require('express');
var router = express.Router();
var auth = require("../lib/auth");
var migration = require("../lib/migration");
require('dotenv').config();
var slug = process.env.API_SLUG;
var usersController = require('../controllers/users.js');
var middleware = require('../controllers/middleware.js');
var customersController = require('../controllers/customer.js')
var medicalHistoryController = require('../controllers/medicalHistory.js')
var drugsController = require('../controllers/drug.js');
var treatmentsController = require('../controllers/treatment.js');
var physicalExaminationController = require('../controllers/physicalExamination.js')
var medicalDiagnosisController = require('../controllers/medicalDiagnosis.js')
var medicinePrescriptionController = require('../controllers/medicinePrescription.js')
var medicalCertificateController = require('../controllers/medicalCertificate')



/**
 * healthcheck
 */
router.get('/health', function(req, res, next) {
    console.log('checked');
    res.send('checked');
});

/**
 * Route migration
 */
router.get('/migration', function(req, res, next) {
    console.log('migration db..');
    var status = migration.task();
    if (!status) return res.status(500).json({ status:'error', message: 'Something gone wrong when migration' });
    return res.status(200).json({ status:'success', message: 'migration successfully' });
});

/**
 * Route login
 */
router.post('/auth', usersController.auth);

router.get('/users', middleware.isAuthenticated,usersController.getAll);

router.get('/users/:id',  middleware.isAuthenticated,usersController.getById);

// router.post('/users', usersController.create);

router.post('/users', middleware.isAuthenticated, usersController.create);

router.put('/users/:id', middleware.isAuthenticated, usersController.update);

router.delete('/users/:id',  middleware.isAuthenticated, usersController.delete);

router.get('/customers', middleware.isAuthenticated, customersController.getAll);

router.get('/customers/:id',  middleware.isAuthenticated,customersController.getById);

router.post('/customers', middleware.isAuthenticated, customersController.create);

router.put('/customers/:id', middleware.isAuthenticated, customersController.update);

router.post('/medicalHistory/customer/:id', middleware.isAuthenticated, medicalHistoryController.createByCustomerId);

router.get('/medicalHistory/customer/:id', middleware.isAuthenticated, medicalHistoryController.getAllByCustomerId);

router.get('/medicalHistory/:id',  middleware.isAuthenticated, medicalHistoryController.getById);

// router.get('/medicalHistory/customer/:customerId/',  middleware.isAuthenticated, medicalHistoryController.getByMedicalHistoryId);

router.put('/medicalHistory/:id', middleware.isAuthenticated, medicalHistoryController.update);

router.post('/drugs', middleware.isAuthenticated, drugsController.create);

router.get('/drugs', middleware.isAuthenticated,drugsController.getAll);

router.get('/drugs/:id',  middleware.isAuthenticated,drugsController.getById);

router.put('/drugs/:id', middleware.isAuthenticated, drugsController.update);

router.delete('/drugs/:id',  middleware.isAuthenticated, drugsController.delete);

router.post('/physicalExamination/customer/:id', middleware.isAuthenticated, physicalExaminationController.createByCustomerId);

router.get('/physicalExamination/customer/:id', middleware.isAuthenticated, physicalExaminationController.getAllByCustomerId);

router.get('/physicalExamination/:id',  middleware.isAuthenticated, physicalExaminationController.getById);

router.put('/physicalExamination/:id', middleware.isAuthenticated, physicalExaminationController.update);

router.post('/medicalDiagnosis/customer/:id', middleware.isAuthenticated, medicalDiagnosisController.createByCustomerId);

router.get('/medicalDiagnosis/customer/:id', middleware.isAuthenticated, medicalDiagnosisController.getAllByCustomerId);

router.get('/medicalDiagnosis/:id',  middleware.isAuthenticated, medicalDiagnosisController.getById);

router.put('/medicalDiagnosis/:id', middleware.isAuthenticated, medicalDiagnosisController.update);

router.post('/treatments/customer/:id', middleware.isAuthenticated, treatmentsController.createByCustomerId);

router.get('/treatments/customer/:id', middleware.isAuthenticated, treatmentsController.getAllByCustomerId);

router.get('/treatments/:id', middleware.isAuthenticated, treatmentsController.getById);

router.put('/treatments/:id', middleware.isAuthenticated, treatmentsController.update);

router.post('/medicinePrescription/customer/:id/drug/:drugId', middleware.isAuthenticated, medicinePrescriptionController.createByCustomerId);

router.get('/medicinePrescription/customer/:id', middleware.isAuthenticated, medicinePrescriptionController.getAllByCustomerId);

router.get('/medicinePrescription/:id',  middleware.isAuthenticated, medicinePrescriptionController.getById);

router.get('/medicinePrescription/customer/:customerId/medicalHistoryId/:id',  middleware.isAuthenticated, medicinePrescriptionController.getMedicineList);

router.put('/medicinePrescription/:id', middleware.isAuthenticated, medicinePrescriptionController.update);

router.delete('/medicinePrescription/:id', middleware.isAuthenticated, medicinePrescriptionController.delete);

router.post('/medicalCertificate/customer/:id', middleware.isAuthenticated, medicalCertificateController.createByCustomerId);

// router.get('/medicineList/customer/:customerId',  middleware.isAuthenticated, medicineListController.getMedicineList);

module.exports = router;
